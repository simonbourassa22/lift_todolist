package ca.polymtl.snippet

import net.liftweb._
import http._
import util.ClearClearable
import util.Helpers._
import js._
import JsCmds._

import ca.polymtl.model.Task


object TaskSnippetDynamique
{       
  val template = Templates( "templates-hidden" :: "ajax-task" :: Nil ).
    openOr( throw new Exception( "cannot load template" ) )

  def renderTask( task: Task ) =
  {
    "li [id]" #> ("task" + task.id) &
    "span *" #> task.label &
    "name=taskId [value]" #> task.id  &
    "#delete" #> SHtml.hidden(delete _)
  }


  def renderTaskList = 
  { 
    "li" #> Task.taskList.map( renderTask _ )
  }
  

  def delete() =
  {
    val boxedId = for {
      paramId <- S.param( "taskId" )
      id <- asLong( paramId )
    } yield id

    boxedId.map( id => {
      println( "Tache eliminee: " + Task.find( id ) + " " + id )
      Task.delete( id )

      Run("""$("#task%d").remove()""" format id)
    }).openOr(Alert("Id don't exist !"))
  }

  def showTaskList = "ul *" #> renderTaskList( template )


  def newTask = 
  {
    val id = "creer"
    var label = ""

    def process() =
    {
      val task = Task(label).add()
      println("Nouvelle tache: " + task.label + " " + task.id)

      Run("$('#taskList').prepend('<li id=new></li>')") &
      Replace("new", renderTask( task )(template)) &
      Focus( id ) &
      SetValById( id,"")
    }

    "name=nom" #> (
      SHtml.text(label,label = _, ("id" -> id )) ++
      SHtml.hidden(process _)
    )
  }
}