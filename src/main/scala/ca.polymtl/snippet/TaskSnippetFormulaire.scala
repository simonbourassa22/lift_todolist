package ca.polymtl.snippet

import net.liftweb._
import http._
import util.Helpers._

import ca.polymtl.model._


object TaskSnippetFormulaire
{

  def taskUrl = S.request.map( _.uri ).openOr( "/" )

  def showTaskList =
  {
    "li" #> Task.taskList.map(renderTask _)
  }

  def renderTask(task: Task) =
  {
     "span *" #> task.label &
     "name=taskId [value]" #> task.id
  }

  def deleteTask =
  {
    var id = 0

    def process(){ 
      println("Tâche éliminée: " + Task.find(id).label + " " + id )
      Task.delete(id)
      S.redirectTo( taskUrl )
    }

    "name=taskId" #> SHtml.onSubmit(x => id = x.toInt) & 
    "type=submit" #> SHtml.onSubmitUnit(process)
  }


  def newTask =
  {
    var label = "tâche"

    def process()
    {
      Task(label).add()
      println("New task: " + label)

      S.redirectTo( taskUrl )
    }

    "name=nom" #> SHtml.onSubmit(label = _) &
    "type=submit" #> SHtml.onSubmitUnit(process)
  }
}