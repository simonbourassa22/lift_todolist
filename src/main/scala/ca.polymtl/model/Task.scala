package ca.polymtl.model

import scala.collection.mutable.ArrayBuffer

case class Task(label: String)
{
  val id = Task.id 
  def add() = { 
    Task.add(this)
    this 
  }
}

object Task
{
  var id: Long = 0
  val taskList = ArrayBuffer[Task]()
 
  def all: List[Task] = taskList.toList
  
  private def add(newTask: Task) =
  {
    id += 1
    taskList += newTask
  }
  
  def find(ident: Long): Task =
  {
    taskList.find(_.id == ident) getOrElse Task("none")
  }

  def delete(id: Long)
  {
    taskList.remove(taskList.indexWhere(_.id == id))
  }
}

