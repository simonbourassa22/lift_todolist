package bootstrap.liftweb

import net.liftweb._
import common._
import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.sitemap._
import Loc._

import ca.polymtl.snippet._

class Boot {
  def boot {

		LiftRules.addToPackages("ca.polymtl")

		val entries = List(
			Menu("Accueil") / "index",
			Menu("Formulaire") / "taches" / "formulaire",
		  Menu("Ajax") / "taches" / "ajax"
		)

		LiftRules.setSiteMap(SiteMap(entries:_*))

		LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

		LiftRules.htmlProperties.default.set( (r: Req) =>
			new Html5Properties(r.userAgent)
		)
	}
}
