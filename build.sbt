name := "todo lift"

version := "0.0.1"

organization := "ca.polymtl"

scalaVersion := "2.9.2"

seq( webSettings :_* )

scalacOptions ++= Seq("-deprecation", "-unchecked")

libraryDependencies ++= {
	val liftVersion = "2.5-M2"
	Seq(
		"net.liftweb"					%% "lift-webkit"		    % liftVersion			            % "compile",
		"org.eclipse.jetty"	 		    %  "jetty-webapp"			% "7.5.4.v20111024"	                % "container",
		"org.scalatest"				    %% "scalatest"				% "2.0.M3"				            % "test"
  	)
}

port in container.Configuration := 8080